import argparse
import os

from cnn import train

LEARNING_RATE: float = 0.01
NESTEROV_GAMMA: float = 0.95
BATCH_SIZE: int = 20


def main() -> None:
    parser = argparse.ArgumentParser()
    parser.add_argument('parameters', metavar='parameters')
    args = parser.parse_args()
    parameters_file_name: str = args.parameters

    cache_file_name: str = f'{parameters_file_name}-metrics-cache.pkl'
    try:
        os.remove(cache_file_name)
    except FileNotFoundError:
        pass

    train(
        lr=LEARNING_RATE,
        gamma=NESTEROV_GAMMA,
        batch_size=BATCH_SIZE,
        parameters_file_name=parameters_file_name)


if __name__ == '__main__':
    main()
