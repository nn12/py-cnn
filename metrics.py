import argparse
import pickle
import sys

import numpy as np
from matplotlib import pyplot
from tqdm import tqdm
from sklearn import metrics

from cnn import Params, load_mnist, classify, NUMBER_OF_CLASSES


def plot_cost(cost: list[float], accuracy: float) -> None:
    pyplot.clf()
    pyplot.plot(cost)
    pyplot.title(f'Overall accuracy: {accuracy:.2f}%')
    pyplot.xlabel('Batches')
    pyplot.ylabel('Cost')
    pyplot.legend(('Train cost',))
    pyplot.savefig('img/cost_accuracy.png')


def compute_confusion_matrix(
        test_data: np.ndarray,
        test_target: np.ndarray,
        params: Params) -> tuple[np.ndarray, list[tuple[int, np.ndarray]], float]:
    matrix: np.ndarray = np.zeros(shape=(NUMBER_OF_CLASSES, NUMBER_OF_CLASSES))
    sample: list[tuple[int, np.ndarray]] = []

    progressbar: tqdm = tqdm(range(len(test_data)), file=sys.stdout)
    correct: float = 0.0

    for iteration in progressbar:
        data: np.ndarray = test_data[iteration]

        output, confidence = classify(data, params)

        target: int = test_target[iteration]
        matrix[output][target] += 1
        sample.append((target, confidence))

        if output == target:
            correct += 1

        progressbar.set_description(f'Accuracy: {float(correct / (iteration + 1)) * 100:0.2f}%')

    return matrix.astype(np.int32), sample, correct / len(test_data)


def plot_confusion_matrix(mat: np.ndarray) -> None:
    fig, ax = pyplot.subplots()

    ax.matshow(mat, cmap='Wistia')
    for i in range(mat.shape[0]):
        for j in range(mat.shape[1]):
            ax.text(i, j, f'{int(mat[j][i])}', va='center', ha='center')

    ax.set_xlabel('Expected')
    ax.xaxis.set_label_position('top')
    ax.set_ylabel('Actual')
    ax.set_xticks(list(range(NUMBER_OF_CLASSES)))
    ax.set_yticks(list(range(NUMBER_OF_CLASSES)))
    fig.savefig('img/confusion_matrix.png')


Metrics = dict[str, float]


def compute_metrics(confusion_matrix: np.ndarray) -> list[Metrics]:
    model_metrics: list[Metrics] = []

    for cls in range(NUMBER_OF_CLASSES):
        tp: float = confusion_matrix[cls, cls]
        fp: float = confusion_matrix[cls, :].sum() - tp
        fn: float = confusion_matrix[:, cls].sum() - tp
        # tn: float = confusion_matrix.sum() - tp - fp - fn

        precision: float = tp / (tp + fp)
        recall: float = tp / (tp + fn)
        f1_score: float = 2 * precision * recall / (precision + recall)

        model_metrics.append(dict(
            precision=precision,
            recall=recall,
            f1_score=f1_score
        ))

    return model_metrics


def plot_metrics(model_metrics: list[Metrics]) -> None:
    precision: list[float] = [model_metrics[cls]['precision'] * 100 for cls in range(NUMBER_OF_CLASSES)]
    recall: list[float] = [model_metrics[cls]['recall'] * 100 for cls in range(NUMBER_OF_CLASSES)]
    f1_score: list[float] = [model_metrics[cls]['f1_score'] for cls in range(NUMBER_OF_CLASSES)]

    classes: list[int] = list(range(NUMBER_OF_CLASSES))

    pyplot.clf()
    pyplot.bar(classes, precision)
    pyplot.title('Precision')
    pyplot.xlabel('Class')
    pyplot.xticks(classes)
    pyplot.ylabel('Precision')
    pyplot.yticks(ticks=[0, 20, 40, 60, 80, 100], labels=[f'{it}%' for it in [0, 20, 40, 60, 80, 100]])
    pyplot.savefig('img/precision.png')

    pyplot.clf()
    pyplot.bar(classes, recall)
    pyplot.title('Recall')
    pyplot.xlabel('Class')
    pyplot.xticks(classes)
    pyplot.ylabel('Recall')
    pyplot.yticks(ticks=[0, 20, 40, 60, 80, 100], labels=[f'{it}%' for it in [0, 20, 40, 60, 80, 100]])
    pyplot.savefig('img/recall.png')

    pyplot.clf()
    pyplot.bar(classes, f1_score)
    pyplot.title('F1-score')
    pyplot.xlabel('Class')
    pyplot.xticks(classes)
    pyplot.ylabel('F1-score')
    pyplot.savefig('img/f1_score.png')


def compute_class_examples_count(test_target: np.ndarray) -> list[int]:
    counts: list[int] = [0 for _ in range(NUMBER_OF_CLASSES)]
    for i in range(len(test_target)):
        counts[test_target[i]] += 1

    return counts


def roc_curve(
        cls: int,
        positive_examples_count: int,
        negative_examples_count: int,
        sample: list[tuple[int, np.ndarray]]) -> tuple[list[float], list[float], float]:
    fpr: list[float] = [0]
    tpr: list[float] = [0]
    auc: float = 0

    sample: list[tuple[int, np.ndarray]] = sorted(sample, key=lambda it: -it[1][cls])

    for i in range(len(sample)):
        if sample[i][0] == cls:  # positive example
            fpr.append(fpr[-1])
            tpr.append(tpr[-1] + 1 / positive_examples_count)
        else:
            fpr.append(fpr[-1] + 1 / negative_examples_count)
            tpr.append(tpr[-1])
            auc += tpr[-1] / negative_examples_count

    return fpr, tpr, auc


def plot_roc_curves(
        examples_count: list[int],
        sample: list[tuple[int, np.ndarray]],
        show: bool = False):
    pyplot.clf()
    aucs: list[float] = []
    for cls in range(NUMBER_OF_CLASSES):
        fpr, tpr, auc = roc_curve(cls, examples_count[cls], sum(examples_count) - examples_count[cls], sample)
        pyplot.plot(fpr, tpr)
        aucs.append(auc)
    pyplot.plot([i / 1000 for i in range(1001)], [i / 1000 for i in range(1001)], '--')
    pyplot.legend([f'{cls} (AUC = {aucs[cls]:.5f})' for cls in range(NUMBER_OF_CLASSES)])
    pyplot.title('ROC Curves')
    pyplot.xlabel('False Positive Rate')
    pyplot.ylabel('True Positive Rate')
    pyplot.savefig('img/roc.png')
    if show:
        pyplot.show()


def plot_roc_curves_sk(
        sample: list[tuple[int, np.ndarray]],
        show: bool = False):
    pyplot.clf()
    aucs: list[float] = []
    for cls in range(NUMBER_OF_CLASSES):
        true_labels, scores = as_binary(cls, sample)
        fpr, tpr, _ = metrics.roc_curve(true_labels, scores)
        auc = metrics.auc(fpr, tpr)
        pyplot.plot(fpr, tpr)
        aucs.append(auc)
    pyplot.plot([i / 1000 for i in range(1001)], [i / 1000 for i in range(1001)], '--')
    pyplot.legend([f'{cls} (AUC = {aucs[cls]:.5f})' for cls in range(NUMBER_OF_CLASSES)])
    pyplot.title('ROC Curves (via sklearn.metrics.roc_curve and sklearn.metrics.auc)')
    pyplot.xlabel('False Positive Rate')
    pyplot.ylabel('True Positive Rate')
    pyplot.savefig('img/roc_sk.png')
    if show:
        pyplot.show()


def as_binary(
        cls: int,
        sample: list[tuple[int, np.ndarray]]) -> tuple[list[int], list[float]]:
    truth_labels: list[int] = [1 if cls == it[0] else 0 for it in sample]
    scores: list[float] = [it[1][cls] for it in sample]

    return truth_labels, scores


def main() -> None:
    parser = argparse.ArgumentParser()
    parser.add_argument('parameters', metavar='parameters', type=str)
    args = parser.parse_args()
    parameters_file_name: str = args.parameters

    with open(parameters_file_name, 'rb') as parameters_file:
        params, cost = pickle.load(parameters_file)

    cache_file_name: str = f'{parameters_file_name}-metrics-cache.pkl'
    try:
        with open(cache_file_name, 'rb') as cache_file:
            confusion_matrix, sample, accuracy, examples_count = pickle.load(cache_file)
    except FileNotFoundError:
        _, (test_data, test_target) = load_mnist()
        confusion_matrix, sample, accuracy = compute_confusion_matrix(test_data, test_target, params)
        examples_count: list[int] = compute_class_examples_count(test_target)

        with open(cache_file_name, 'wb') as cache_file:
            pickle.dump([confusion_matrix, sample, accuracy, examples_count], cache_file)

    plot_confusion_matrix(confusion_matrix)

    plot_cost(cost, accuracy * 100)

    model_metrics: list[Metrics] = compute_metrics(confusion_matrix)

    plot_metrics(model_metrics)

    plot_roc_curves(examples_count, sample)
    plot_roc_curves_sk(sample)


if __name__ == '__main__':
    main()
