import numpy as np
from numba import njit

from cnn.utils import Params, Gradients, Vs, NUMBER_OF_CLASSES
from cnn.forward import feed_forward
from cnn.gradients import compute_gradients


@njit(cache=True)
def to_one_hot(cls: int) -> np.ndarray:
    code: np.ndarray = np.zeros(shape=(NUMBER_OF_CLASSES, 1))
    code[cls][0] = 1.0
    return code


@njit(cache=True)
def cross_entropy(p: np.ndarray, q: np.ndarray) -> float:
    return -(q * np.log(p)).sum()


def update(
        params: Params,
        v: Vs,
        gradients: Gradients,
        gamma: float,
        lr: float,
        batch_size: int) -> None:
    for i in range(len(params)):
        v[i] = gamma * v[i] + lr * gradients[i] / batch_size
        params[i] -= v[i]


def create_accumulator(params: Params) -> Gradients:
    return [np.zeros_like(params[i]) for i in range(len(params))]


def accumulate_gradients(
        accumulator: Gradients,
        gradients: Gradients) -> None:
    for i in range(len(accumulator)):
        accumulator[i] += gradients[i]


def nesterov_gd(
        batch_data: np.ndarray,
        batch_target: np.ndarray,
        lr: float, gamma: float,
        params: Params,
        vs: Vs) -> (Params, Vs, float):
    cost: float = 0
    batch_size = len(batch_data)

    gradients: list[np.ndarray] = create_accumulator(params)

    for i in range(batch_size):
        data: np.ndarray = batch_data[i]
        target: np.ndarray = to_one_hot(batch_target[i])

        outputs = feed_forward(data, params)
        current_gradients: Gradients = compute_gradients(data, target, outputs, params)

        accumulate_gradients(gradients, current_gradients)

        cost += cross_entropy(outputs[-1], target)

    update(params, vs, gradients, gamma, lr, batch_size)

    cost = cost / batch_size

    return params, vs, cost
