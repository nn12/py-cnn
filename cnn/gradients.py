import numpy as np
from numba import njit

from cnn.utils import Params, Gradients, Outputs


@njit(cache=True)
def cross_corr_bp(
        errors: np.ndarray,
        image: np.ndarray,
        filters: np.ndarray,
        stride: int) -> tuple[np.ndarray, np.ndarray]:
    filters_count, filter_channels, filter_size, _ = filters.shape
    _, image_size, _ = image.shape

    next_errors: np.ndarray = np.zeros_like(image)
    gradients: np.ndarray = np.zeros_like(filters)

    for filter_index in range(filters_count):
        filter: np.ndarray = filters[filter_index]
        gradient: np.ndarray = gradients[filter_index]
        error: np.ndarray = errors[filter_index]

        y: int = 0
        for offset_y in range(0, image_size - filter_size + 1, stride):
            x: int = 0
            for offset_x in range(0, image_size - filter_size + 1, stride):
                # evil math
                gradient += error[y, x] * image[:, offset_y:offset_y + filter_size, offset_x:offset_x + filter_size]
                next_errors[:, offset_y:offset_y + filter_size, offset_x:offset_x + filter_size] += error[y, x] * filter
                x += 1
            y += 1

    return next_errors, gradients


@njit(cache=True)
def argmax2d(arr: np.ndarray) -> tuple[int, int]:
    idx: int = arr.argmax()
    return idx // arr.shape[1], idx % arr.shape[1]


@njit(cache=True)
def maxpool_bp(
        errors: np.ndarray,
        image: np.ndarray,
        patch_size: int,
        stride: int) -> np.ndarray:
    channels_count, image_size, _ = image.shape

    next_errors: np.ndarray = np.zeros_like(image)

    for channel_index in range(channels_count):
        channel: np.ndarray = image[channel_index]
        channel_error: np.ndarray = next_errors[channel_index]
        error: np.ndarray = errors[channel_index]

        out_y: int = 0
        for curr_y in range(0, image_size - patch_size + 1, stride):
            out_x: int = 0
            for curr_x in range(0, image_size - patch_size + 1, stride):
                (y, x) = argmax2d(channel[curr_y:curr_y + patch_size, curr_x:curr_x + patch_size])
                channel_error[curr_y + y, curr_x + x] = error[out_y, out_x]
                out_x += 1
            out_y += 1

    return next_errors


def dense_bp(
        error: np.ndarray,
        layer: np.ndarray,
        layer_in: np.ndarray) -> tuple[np.ndarray, np.ndarray]:
    grad = error.dot(layer_in.T)
    error = layer.T.dot(error)

    return error, grad


def compute_gradients(
        image: np.ndarray,
        target: np.ndarray,
        outputs: Outputs,
        params: Params) -> Gradients:
    conv1, conv2, fc1, fc2 = params
    conv1_out, conv2_out, downsampled, flattened, fc1_out, result = outputs

    error: np.ndarray = result - target

    error, fc2_grad = dense_bp(error, fc2, fc1_out)
    error[fc1_out <= 0] = 0

    error, fc1_grad = dense_bp(error, fc1, flattened)
    error = error.reshape(downsampled.shape)

    error = maxpool_bp(error, conv2_out, patch_size=2, stride=2)
    error[conv2_out <= 0] = 0

    error, conv2_grad = cross_corr_bp(error, conv1_out, conv2, stride=1)
    error[conv1_out <= 0] = 0

    _, conv1_grad = cross_corr_bp(error, image, conv1, stride=1)

    grads = [conv1_grad, conv2_grad, fc1_grad, fc2_grad]

    return grads
