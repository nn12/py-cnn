import numpy as np
from numba import njit

from cnn.utils import Params, Outputs


@njit(cache=True)
def cross_corr(
        image: np.ndarray,
        filters: np.ndarray,
        stride: int) -> np.ndarray:
    filters_count, filter_channels, filter_size, _ = filters.shape
    image_channels, image_size, _ = image.shape

    feature_map_size: int = (image_size - filter_size) // stride + 1
    feature_maps: np.ndarray = np.zeros(shape=(filters_count, feature_map_size, feature_map_size))

    for filter_index in range(filters_count):
        filter: np.ndarray = filters[filter_index]
        feature_map: np.ndarray = feature_maps[filter_index]

        y: int = 0
        for offset_y in range(0, image_size - filter_size + 1, stride):
            x: int = 0
            for offset_x in range(0, image_size - filter_size + 1, stride):
                feature_map[y][x] = \
                    (filter * image[:, offset_y:offset_y + filter_size, offset_x:offset_x + filter_size]).sum()
                x += 1
            y += 1

    return feature_maps


@njit(cache=True)
def maxpool(
        image: np.ndarray,
        patch_size: int,
        stride: int):
    image_channels, image_size, _ = image.shape
    downsampled_size: int = (image_size - patch_size) // stride + 1
    downsampled: np.ndarray = np.zeros(shape=(image_channels, downsampled_size, downsampled_size))

    for channel_index in range(image_channels):
        channel: np.ndarray = image[channel_index]
        downsampled_channel: np.ndarray = downsampled[channel_index]

        y: int = 0
        for offset_y in range(0, image_size - patch_size + 1, stride):
            x: int = 0
            for offset_x in range(0, image_size - patch_size + 1, stride):
                downsampled_channel[y, x] = \
                    channel[offset_y:offset_y + patch_size, offset_x:offset_x + patch_size].max()
                x += 1
            y += 1

    return downsampled


@njit(cache=True)
def softmax(z: np.ndarray) -> np.ndarray:
    exp: np.ndarray = np.exp(z)
    return exp / exp.sum()


def feed_forward(
        image: np.ndarray,
        params: Params) -> Outputs:
    conv1, conv2, fc1, fc2 = params

    conv1_out: np.ndarray = cross_corr(image, conv1, stride=1)
    conv1_out[conv1_out <= 0] = 0

    conv2_out: np.ndarray = cross_corr(conv1_out, conv2, stride=1)
    conv2_out[conv2_out <= 0] = 0
    downsampled: np.ndarray = maxpool(conv2_out, patch_size=2, stride=2)

    maps_count, map_size, _ = downsampled.shape
    flattened: np.ndarray = downsampled.reshape((maps_count * map_size * map_size, 1))

    fc1_out: np.ndarray = fc1.dot(flattened)
    fc1_out[fc1_out <= 0] = 0

    fc2_out: np.ndarray = fc2.dot(fc1_out)
    result: np.ndarray = softmax(fc2_out)

    return [conv1_out, conv2_out, downsampled, flattened, fc1_out, result]


def classify(image: np.ndarray, params: Params) -> tuple[int, np.ndarray]:
    result: np.ndarray = feed_forward(image, params)[-1]
    return result.argmax(), result
