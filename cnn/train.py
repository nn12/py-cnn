import pickle
import sys

import numpy as np
from tqdm import tqdm

from cnn.gradient_descent import nesterov_gd
from cnn.utils import Params, Vs, load_mnist


def create_filters(size: tuple[int, ...], scale: float = 1.0) -> np.ndarray:
    stddev: float = scale / np.sqrt(np.array(size).prod())
    return np.random.normal(loc=0, scale=stddev, size=size)


def initialize_weights(size: tuple[int, ...]) -> np.ndarray:
    return np.random.standard_normal(size=size) * 0.01


def train(
        lr: float,
        gamma: float,
        batch_size: int,
        parameters_file_name: str) -> None:
    (data, target), _ = load_mnist()

    batches: list[tuple[np.ndarray, np.ndarray]] = [
        (data[i:i + batch_size], target[i:i + batch_size])
        for i in range(0, len(data), batch_size)
    ]

    filters1: np.ndarray = create_filters((7, 1, 5, 5))  # (f. count, ch. count, width, height)
    filters2: np.ndarray = create_filters((6, 7, 5, 5))
    fc1: np.ndarray = initialize_weights((150, 600))  # (out count, in count)
    fc2: np.ndarray = initialize_weights((10, 150))

    params: Params = [filters1, filters2, fc1, fc2]

    cost: list[float] = []

    print(f'LR:{lr}, Batch Size:{batch_size}')

    vs: Vs = [np.zeros_like(filters1), np.zeros_like(filters2), np.zeros_like(fc1), np.zeros_like(fc2)]

    progressbar = tqdm(batches, file=sys.stdout)
    for batch in progressbar:
        data, target = batch
        params, vs, current_cost = nesterov_gd(data, target, lr, gamma, params, vs)
        cost.append(current_cost)
        progressbar.set_description(f'Cost: {current_cost:.2f}')

    with open(parameters_file_name, 'wb') as parameters_file:
        pickle.dump([params, cost], parameters_file)
