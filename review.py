import argparse
import pickle

import numpy as np

from cnn import Params, load_mnist, classify, IMAGE_SIZE, shuffle


def show_image(img: np.ndarray) -> None:
    img = img.reshape(IMAGE_SIZE, IMAGE_SIZE)
    for row in img:
        for value in row:
            c: str = ' '
            if value > 0.3:
                c = '.'
            if value > 0.6:
                c = '*'
            if value > 0.9:
                c = '@'
            print(c, end='')
        print()


def review(
        test_data: np.ndarray,
        test_target: np.ndarray,
        params: Params) -> None:
    correct: int = 0

    for i in range(len(test_data)):
        data: np.ndarray = test_data[i]

        output, confidence = classify(data, params)

        target: int = test_target[i]

        print(f'Image {i + 1}/{len(test_data)}')
        show_image(data)
        print(f'Expected: {target}')
        print(f'Actual:   {output}')

        if output == target:
            correct += 1
            print('Yay!')

        print(f'Accuracy: {correct / (i + 1) * 100:.2f}%')
        try:
            input('Show more?')
        except EOFError:
            break


def main() -> None:
    parser = argparse.ArgumentParser()
    parser.add_argument('parameters', metavar='parameters', type=str)
    args = parser.parse_args()
    parameters_file_name: str = args.parameters

    with open(parameters_file_name, 'rb') as parameters_file:
        params, cost = pickle.load(parameters_file)

    print('Loading test data...')
    _, (test_data, test_target) = load_mnist()
    test_data, test_target = shuffle(test_data, test_target)
    review(test_data, test_target, params)


if __name__ == '__main__':
    main()
